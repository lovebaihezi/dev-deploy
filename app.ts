import { oak } from './deps.ts';
import { rolling } from './utils/mod.ts';

type Context = oak.Context<{
    log: typeof rolling;
}, {
    log: typeof rolling;
}>;

const deployType = ['Source', 'Package'] as const;

const deployValueCheck = {
    [deployType[0]]: (source: unknown): DeploySource => {
        if (typeof source !== 'object') {
            throw new TypeError(
                `the "value" field of deploy is not "object", it is ${typeof source}`,
            );
        }
        const value = source as Record<string, unknown>;
        for (const key of ['username', 'repoName', 'host'] as const) {
            if (
                typeof value[key] !== 'string'
            ) {
                throw new TypeError(
                    `The ${key} field type of deploy source is not "string", it's ${typeof value[
                        key
                    ]}`,
                );
            }
        }
        for (const key of ['branch', 'commitHash'] as const) {
            if (
                typeof value[key] !== 'string' &&
                typeof value[key] !== 'undefined'
            ) {
                throw new TypeError(
                    `The ${key} field type of deploy source is neither "string" or "undefined", it's ${typeof value[
                        key
                    ]}`,
                );
            }
        }
        return value as DeploySource;
    },
    [deployType[1]]: (value: unknown) => {
        if (typeof value !== 'string') {
            throw new TypeError(
                'deploy type is package, but its value can not convert into URL',
            );
        }
        return new URL(value as string);
    },
} as const;

type DeployType = typeof deployType[number];

type DeploySource = {
    username: string;
    repoName: string;
    host: string;
    branch?: 'main' | 'master' | string;
    commitHash?: string;
};

type Deploy = {
    type: 'Source';
    value: DeploySource;
} | { type: 'Package'; value: URL };

type Option = {
    openOptions?: Deno.OpenOptions | undefined;
    mkDirOption?: Deno.MkdirOptions | undefined;
    // onClose?: (
    //     file: Deno.FsFile,
    // ) => void | ((file: Deno.FsFile) => Promise<void>);
};

type ToggleBuildDir = (
    rootDir: URL | string,
    option?: Option,
) => Promise<
    (deploy: Deploy) => string | URL
>;

const toggleBuildDir: ToggleBuildDir = async (
    rootDirURL,
    options?,
) => {
    if (
        !await Deno.open(rootDirURL).then((file) => {
            file.close();
            return true;
        }).catch(() => false)
    ) {
        await Deno.mkdir(rootDirURL, options?.mkDirOption);
    }
    return (deploy) => {
        throw new Error("unimplemented");
    };
};

const DeployTypeCheck: (source: unknown) => Deploy = (source) => {
    if (typeof source !== 'object' || source === null) {
        throw new TypeError(`The "deploy" is not an object!`);
    }
    const record = source as Record<string, unknown>;
    if (Object.hasOwn(record, 'type') && typeof record.type !== 'string') {
        throw new TypeError(`The "deploy" lack of a "type" field!`);
    }
    if (
        !deployType.includes(record.type as DeployType)
    ) {
        throw new TypeError(
            `The "type" field in "deploy" is neither ${
                deployType.map((s) => `"${s}"`).join(' or ')
            }, it is "${record.type}"`,
        );
    }
    if (typeof record.value !== 'object' && typeof record.value !== 'string') {
        throw new TypeError(
            `The "value" field in "deploy" is neither "object" or "string", it is "${typeof record
                .value}"!`,
        );
    }
    const deploy = record as Deploy;
    deployValueCheck[deploy.type](deploy.value);
    return deploy as Deploy;
};

const app = new oak.Application({ logErrors: true, state: { log: rolling } });

const buildDir = await toggleBuildDir('./deploy');

app.use(async (ctx: Context) => {
    try {
        const { method, url } = ctx.request;
        if (method === 'POST' && url.pathname.match(/\/deploy/)) {
            const body = ctx.request.body();
            if (body.type === 'json') {
                try {
                    const deploy = DeployTypeCheck(await body.value);
                    const url = buildDir(deploy);
                } catch (e) {
                    if (e instanceof Error) {
                        ctx.response.body = e.message;
                    } else {
                        ctx.response.status = 500;
                        ctx.response.body = 'unknown error!';
                    }
                }
            } else {
                throw new TypeError(
                    `incorrect request body type: ${body.type}!`,
                );
            }
        }
    } catch (e: unknown) {
        console.error(e);
        if (e instanceof Error) {
            ctx.response.body = e.message;
        }
    }
});

app.listen({ port: 8081 });
