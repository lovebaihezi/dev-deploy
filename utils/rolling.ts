export const pipeTo: (
    readAbleStream: ReadableStream,
    writeAbleStream: WritableStream,
    writeOption?: PipeOptions,
) => Promise<void> = async (
    readAbleStream,
    writeAbleStream,
    writeOption,
) => {
    if (readAbleStream.locked) {
        throw new Error('can not read from read able stream!', {
            cause: new Error('it\'s locked!'),
        });
    }
    while (writeAbleStream.locked);
    await readAbleStream.pipeTo(writeAbleStream, writeOption);
};
